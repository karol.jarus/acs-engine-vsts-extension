#!/bin/bash

# Add comments to explain the components of the script for easier troubleshooting
# Include echo statements so comments are written to output file
# Include necessary error checking

# Local variables

eval "$(echo $1 | sed 's/|/\n/g')"

echo $(date) " - Starting Script"

# Step 1 - example of creating config file
echo $(date) " - Creating vsts-agent.yaml file using local variables"

cat > vsts-agent.yaml <<EOF
apiVersion: v1
kind: ReplicationController
metadata:
  name: vsts-agent
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: vsts-agent
        version: "0.1"
    spec:
      containers:
      - name: vsts-agent
        image: microsoft/vsts-agent:ubuntu-16.04-docker-17.12.0-ce
        env:
          - name: VSTS_ACCOUNT
            value: $VSTS_ACCOUNT
          - name: VSTS_TOKEN
            value: $VSTS_TOKEN
          - name: VSTS_POOL
            value: $VSTS_POOL
        volumeMounts:
        - mountPath: /var/run/docker.sock
          name: docker-volume
      volumes:
      - name: docker-volume
        hostPath:
          path: /var/run/docker.sock
EOF

# Step 23 - example of executing other commands
echo $(date) " - Executing command"

kubectl -f ./vsts-agent.yaml

echo $(date) " - Script complete"